/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio6;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       /**
        * Método que calcula el producto de tres enteros
        */
        int x, y, z, producto, resultado;
        Scanner entrada=new Scanner(System.in);

        System.out.print("Escriba el primer entero: ");
        x=entrada.nextInt();

        System.out.print("Escriba el segundo entero: ");
        y=entrada.nextInt();

        System.out.print("Escriba el tercer entero: ");
        z=entrada.nextInt();

        producto=(x*y)*z;
        resultado=producto;

        System.out.printf("El producto de "+x+"*"+y+"*"+z+" es: %d%n", 
        resultado);

    
    }
    
}
