/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio35;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio35 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Calculadora de ahorro por viajes compartidos en automóvil) Investigue varios sitios Web de viajes compartidos en automóvil. Cree una aplicación que calcule su costo diario al conducir su automóvil, de modo que pueda estimar cuánto dinero puede ahorrar si comparte los viajes en automóvil, lo cual también tiene otras ventajas, como la reducción de las emisiones de carbono y la reducción de la congestión de tráfico. La aplicación debe recibir como entrada la siguiente información y mostrar el costo por día para el usuario por conducir al trabajo:
Total de kilómetros conducidos por día.
Costo por litro de gasolina.
Promedio de kilómetros por litro.
Cuotas de estacionamiento por día.
Peaje por día.
         */
        Scanner s=new Scanner(System.in);
            double KilometrosPorDia, CostoGasolina,KilometrosPorLitro,
               CuotaEstacionamiento, Peaje, Gasto;
       int personas;
       
       System.out.print("¿Cuántos kilómetros condujo hoy? ");
       KilometrosPorDia=s.nextDouble();
       System.out.print("¿Cuál es el costo de gasolina por litro? ");
       CostoGasolina=s.nextDouble();
       System.out.print("¿Cuál es el rendimiento del vehiculo?"
               + " (km/l) ");
       KilometrosPorLitro=s.nextDouble();
       System.out.print("¿Cuál es la cuota de estacionamiento por día? ");
       CuotaEstacionamiento=s.nextDouble();
       System.out.print("¿Cuál es el peaje por día? ");
       Peaje=s.nextDouble();
       
       Gasto=((KilometrosPorDia/KilometrosPorLitro)*CostoGasolina)
               +CuotaEstacionamiento+Peaje;
       
       System.out.printf("Su gasto al día de ir de su casa a ese lugar es "
               + "de: %f%n", Gasto);
       System.out.printf("¿Con cuántas personas piensa compartir el viaje?%n"
               + "¿1, 2, 3 o 4 personas? ");
       personas=s.nextInt();
       
       if (personas == 1)
           System.out.printf("El gasto que le tocaría a cada uno sería de: %f%n",
                   Gasto/2);
       if (personas == 2)
           System.out.printf("El gasto que le tocaría a cada uno sería de: %f%n",
                   Gasto/3);
       if (personas == 3)
           System.out.printf("El gasto que le tocaría a cada uno sería de: %f%n",
                   Gasto/4);
       if (personas == 5)
           System.out.printf("El gasto que le tocaría a cada uno sería de: %f%n",
                   Gasto/5);

    }
    
}
