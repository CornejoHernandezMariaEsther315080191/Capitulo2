/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio31;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio31 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Tabla de cuadrados y cubos) Utilizando sólo las técnicas de programación que aprendió en este capítulo, escriba una aplicación que calcule los cuadrados y cubos de los números del 0 al 10, y que imprima los valores resultantes en formato de tabla.
         */
         System.out.printf("número     cuadrado     cubo%n"
                + "1             1          1%n"
                + "2             4          8%n"
                + "3             9          27%n"
                + "4             16         64%n"
                + "5             25         125%n"
                + "6             36         216%n"
                + "7             49         343%n"
                + "8             64         512%n"
                + "9             81         729%n"
                + "10            100        1000%n");
    }
    
}
