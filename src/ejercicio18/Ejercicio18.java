/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio18;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio18 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Visualización de figuras con asteriscos) 
         * Escriba una aplicación que muestre un cuadro, un óvalo, 
         * una flecha y un diamante usando asteriscos (*)
         */
        
        System.out.println("*********       *           *"
                + "\n*       *     *   *         **        *"
                + "\n*       *     *   *   *********      * *"
                + "\n*       *     *   *         **        *"
                + "\n*********       *           *");
    }
    
}
